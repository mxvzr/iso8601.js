var assert = require('assert');

const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
const DAY = 24 * HOUR;
const WEEK = 7 * DAY;
const MONTH = 30 * DAY;
const YEAR = 365 * DAY;

const UNITS = [SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR];

var reDurationSpec = /^P((\d+)(Y))*((\d+)(M))*((\d+)(W))*((\d+)(D))*(T((\d+)(H))*((\d+)(M))*((\d+)(S))*)*/;

var isDurationSpec = function(duration_spec) {
	return reDurationSpec.test(duration_spec);
};

function Interval(duration_spec) {
	if (!(this instanceof Interval))
		return new Interval(duration_spec);

	assert(isDurationSpec(duration_spec), 'Invalid interval duration spec: ' + duration_spec);

	this.y = 0;
	this.m = 0;
	this.w = 0;
	this.d = 0;
	this.h = 0;
	this.i = 0;
	this.s = 0;

	var matches = duration_spec.match(reDurationSpec),
		timePart = false;

	for (var i = 1; i < matches.length; i = i + 3) {
		if (i === (1 + (3 * 4))) {// offset + (capture group per unit * 4 date units)
			i++; // Skipping time capture group
			timePart = true;	
		}	

		if (matches[i] === undefined)
			continue;
			
		var unit = matches[i + 2],
			value = Number(matches[i + 1].replace(',','.'));
		
		if (timePart && unit === "M")
			unit = "i";
		else
			unit = unit.toLowerCase();
		
		this[unit] = value;
	}
}

// Interval.prototype.format = function([UNITS])

Interval.prototype.valueOf = function() {
	return (SECOND * this.s) + (MINUTE * this.i) + (HOUR * this.h) +
		(DAY * this.d) + (WEEK * this.w) + (MONTH * this.m) + (YEAR * this.y);
};

function Duration(from, to) {
	if (!(this instanceof Duration))
		return new Duration(from, to);
		
	assert(from instanceof Date || from instanceof Interval, 'Invalid "from" argument');
	assert(to instanceof Date || to instanceof Interval, 'Invalid "to" argument');
	assert(!(from instanceof Interval && to instanceof Interval), 'At least one argument must be a date');
	
	if (from instanceof Interval)
		from = new Date(+to - from);
	if (to instanceof Interval)
		to = new Date(+from + to);

	assert(from <= to, 'Argument "from" must be before "to"');

	this.from = from;
	this.to = to;
}

function Period(duration, interval) {
	assert(duration instanceof Duration, 'Invalid "duration" argument');
	interval = interval instanceof Interval ? interval : new Interval(interval);
	assert(interval instanceof Interval, 'Invalid "interval" argument');
	
	var retVal = [duration.from],
		tail = +duration.from;

	while (tail < duration.to) {
		tail += interval;
		retVal.push(new Date(tail));
	}
	return retVal;
}

module.exports = {
	Duration: Duration,
	Interval: Interval,
	Period: Period
};
