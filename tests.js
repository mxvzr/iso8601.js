var assert = require('assert'),
	Interval = require('./lib.js').Interval,
	Duration = require('./lib.js').Duration,
	Period = require('./lib.js').Period;

(function IntervalTest() {
	assert(Interval && Interval.length === 1);
	var one = new Interval('P3Y6M4DT12H30M5S');
	assert(one);
	assert.equal(one.y, 3);
	assert.equal(one.m, 6);
	assert.equal(one.w, 0);
	assert.equal(one.d, 4);
	assert.equal(one.h, 12);
	assert.equal(one.i, 30);
	assert.equal(one.s, 5);

	assert.equal(+one, 110550605000);
})();

(function DurationTest() {
	assert(Duration && Duration.length === 2);
	var one = new Duration(new Date('2010-01-01'), new Interval('P1D'));
	assert(one.from instanceof Date);
	assert(+one.from === +new Date('2010-01-01'));
	assert(one.to instanceof Date);
	assert(+one.to === +new Date('2010-01-02'));
})();

(function PeriodTest() {
	assert(Period && Period.length === 2);
	var jan = new Duration(new Date('2010-01-01'), new Date('2010-01-30'));
	var one = Period(jan, new Interval('PT24H'));
	assert(Array.isArray(one));
	assert(one.length === 30);
})();
