iso8601 friendly date stuff

```
#!javascript

var interval = new Interval("P3YT6H");
// interval.y === 3
// interval.m === 0
// interval.h === 6
// ...
// +interval === millisecs sum

var duration = new Duration(new Date("2010-01-01"), new Interval("P1M"));
// duration.from = Date#2010-01-01
// duration.to = Date#2010-02-01

var period = new Period(duration, new Interval("PT24H"));
// period === [Date#2010-01-01, Date#2010-01-02, ... , Date#2010-02-01]
```
